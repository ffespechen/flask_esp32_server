from flask import Flask, request

app = Flask(__name__)


@app.route('/', methods=['POST'])
def index():
    print('Valores recibidos: {}'.format(request.get_json()))
    return '<h1>ESP32 Server</h1>'
